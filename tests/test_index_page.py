import requests
import config
import allure

@allure.suite("Index Page tests")
class TestIndexPage:
    """ Test for index page"""

    @allure.title("Index page content verification")
    def test_index_page_is_loaded(self):
        """ Test if page is loaded"""

        response = requests.get(config.base_url)
        assert response.status_code == 200
        assert "Hello, World" in response.text